

class Queue<T> {
    private data: Array<T> = [];
    push(item : T) {
        this.data.push(item);
    } 
    
    pop() : T | undefined{
       return this.data.shift();  
    }

    getData(){
        return this.data;
    }
}

let queue : Queue<number> = new Queue<number>();

queue.push(5);
queue.push(10);
queue.push(15);
queue.push(20);
queue.pop();

queue.getData().forEach(el =>{
    console.log(el)
});
