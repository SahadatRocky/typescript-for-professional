
let isPresent : boolean = false;
let magic : number = 66.6;
let hello : string = 'Hello';

let notDefined : undefined = undefined;
let notPresent: null = null;

let penta = Symbol('star');

console.log(isPresent);
console.log(magic);
console.log(hello);
console.log(penta);