
type Point = {x: number, y: number};

const p : Point = {x : 0, y: 0};

p.x = 3;
p.y = 8;
