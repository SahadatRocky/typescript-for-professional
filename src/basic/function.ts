

let add = (a : number , b: number) : number => a+b;

function sum(...values : number[]){
    return values.reduce((previous, current)=>{
        return previous + current;
    },0)
}


console.log(add(2,3));
console.log(sum(1,2,3));
