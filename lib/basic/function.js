"use strict";
let add = (a, b) => a + b;
function sum(...values) {
    return values.reduce((previous, current) => {
        return previous + current;
    }, 0);
}
console.log(add(2, 3));
console.log(sum(1, 2, 3));
