"use strict";
class Queue {
    constructor() {
        this.data = [];
    }
    push(item) {
        this.data.push(item);
    }
    pop() {
        return this.data.shift();
    }
    getData() {
        return this.data;
    }
}
let queue = new Queue();
queue.push(5);
queue.push(10);
queue.push(15);
queue.push(20);
queue.pop();
queue.getData().forEach(el => {
    console.log(el);
});
